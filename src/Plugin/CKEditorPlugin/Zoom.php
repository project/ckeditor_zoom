<?php

namespace Drupal\ckeditor_zoom\Plugin\CKEditorPlugin;

/**
 * @file
 * Definition of \Drupal\ckeditor_zoom\Plugin\CKEditorPlugin\Zoom.
 */

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Zoom" plugin.
 *
 * @CKEditorPlugin(
 *   id = "zoom",
 *   label = @Translation("Zoom")
 * )
 */
class Zoom extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return base_path() . 'libraries/zoom/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getConfig().
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'Zoom' => [
        'label' => $this->t('Zoom'),
        'image_alternative' => [
          '#type' => 'inline_template',
          '#template' => '<a href="#" role="button" aria-label=""><span class="ckeditor-button-dropdown">' . $this->t('Zoom') . '<span class="ckeditor-button-arrow"></span></span></a>',
          '#context' => [
            'zoom' => $this->t('Zoom'),
          ],
        ],
      ],
    ];
  }

}
